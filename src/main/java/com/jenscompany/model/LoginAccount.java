package com.jenscompany.model;

/**
 * Created by jennifer.cheung on 12/28/2016.
 */
public class LoginAccount {
    private String accountId;
    private String baseUrl;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
