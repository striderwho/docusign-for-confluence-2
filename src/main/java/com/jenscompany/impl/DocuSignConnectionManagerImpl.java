package com.jenscompany.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.jenscompany.api.DocuSignConnectionManager;
import com.jenscompany.model.LoginAccount;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.inject.Inject;
import javax.inject.Named;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ExportAsService ({DocuSignConnectionManager.class})
@Named ("DocuSignConnectionManager")
public class DocuSignConnectionManagerImpl implements DocuSignConnectionManager
{
    LoginAccount loginAccount;
    JSONObject authHeader;

    public DocuSignConnectionManagerImpl()
    {
        Map authHeaderItems = new HashMap();
        authHeaderItems.put("Username", DocuSignConnectionManager.USERNAME);
        authHeaderItems.put("Password", DocuSignConnectionManager.PASSWORD);
        authHeaderItems.put("IntegratorKey", DocuSignConnectionManager.INT_KEY);
        authHeader = ConnectionUtilities.buildJSON(authHeaderItems);
        loginAccount = new LoginAccount();
    }

    public LoginAccount login(Map headers) throws Exception {

        HttpsURLConnection conn = initializeRequest(DocuSignConnectionManager.LOGIN_URL+".json", "GET", "");

        JSONObject obj = ConnectionUtilities.decodeJSON(getResponseBody(conn));

        JSONArray loginAccounts = (JSONArray) obj.get("loginAccounts");

        if(loginAccounts.size() <= 0)
            throw new Exception("No login accounts returned");

        JSONObject acct = null;

        // Find the default account
        for(Object acctobj:loginAccounts) {
            JSONObject loginAccount = (JSONObject) acctobj;
            if(loginAccount.get("isDefault").equals("true")) {
                acct = loginAccount;
                break;
            }
        }

        if(acct == null ) {
            throw new Exception("No default account could be found.");
        }

        loginAccount.setAccountId(acct.get("accountId").toString());
        loginAccount.setBaseUrl(acct.get("baseUrl").toString());

        return loginAccount;

        /**
        // login call available off the AuthenticationApi
        AuthenticationApi authApi = new AuthenticationApi();

        // login has some optional parameters we can set
        AuthenticationApi.LoginOptions loginOps = authApi.new LoginOptions();
        loginOps.setApiPassword("true");
        loginOps.setIncludeAccountIdGuid("true");
        LoginInformation loginInfo = authApi.login(loginOps);

        // note that a given user may be a member of multiple accounts
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
        List<LoginAccount> loginAccounts = loginInfo.getLoginAccounts();

        if(loginAccounts.size() <= 0)
            throw new Exception("No login accounts were found for the provided credentials.");

        // Lets just take the first one for now
        loginAccount = loginAccounts.get(0);
         **/
    }

    public HttpsURLConnection initializeRequest(String url, String method, String body) {
        HttpsURLConnection conn = null;
        try {
            conn = (HttpsURLConnection)new URL(url).openConnection();

            conn.setRequestMethod(method);
            conn.setRequestProperty("X-DocuSign-Authentication", authHeader.toJSONString());
            conn.setRequestProperty("Accept", "application/json");
            if (method.equalsIgnoreCase("POST"))
            {
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=BOUNDARY");
                conn.setDoOutput(true);
            }
            else {
                conn.setRequestProperty("Content-Type", "application/json");
            }
            return conn;

        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public String getResponseBody(HttpsURLConnection conn) {
        BufferedReader br = null;
        StringBuilder body = null;
        String line = "";
        try {
            // we use xPath to get the baseUrl and accountId from the XML response body
            int responseCode = conn.getResponseCode();
            br = new BufferedReader(new InputStreamReader( conn.getInputStream()));
            body = new StringBuilder();
            while ( (line = br.readLine()) != null)
                body.append(line);
            return body.toString();
        } catch (Exception e) {
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }

    public String getBaseUrl() {
        return loginAccount.getBaseUrl();
    }

    public String getAccountId() {
        return loginAccount.getAccountId();
    }
}