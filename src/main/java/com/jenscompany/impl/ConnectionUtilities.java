package com.jenscompany.impl;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Jen
 * Date: 29/10/15
 * Time: 14:40
 * To change this template use File | Settings | File Templates.
 */
public class ConnectionUtilities {
    public static JSONObject decodeJSON(String strToDecode) throws ParseException {
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(strToDecode);
        JSONObject objs = (JSONObject)obj;

        return objs;
    }

    public static JSONObject buildJSON(Map items) {
        JSONObject obj = new JSONObject();
        obj.putAll(items);
        return obj;
    }

    public HttpsURLConnection httpConnection(String url) throws Exception {
        URL Url = new URL(url);
        HttpsURLConnection conn = (HttpsURLConnection) Url.openConnection();

        return conn;
    }
}
