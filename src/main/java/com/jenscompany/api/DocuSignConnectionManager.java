package com.jenscompany.api;

import com.jenscompany.model.LoginAccount;

import javax.net.ssl.HttpsURLConnection;
import java.util.Map;

public interface DocuSignConnectionManager
{
    public static String LOGIN_URL = "https://demo.docusign.net/restapi/v2/login_information";
    public static String BASE_PATH = "https://demo.docusign.net/restapi";
    public static String AUTH_HEADER_NAME = "X-DocuSign-Authentication";
    public static String USERNAME = "jennifer.cheung@docusign.com";
    public static String PASSWORD = "S9gnh454damm96!!";
    public static String INT_KEY = "PRIM-a1159a3d-3ea2-44ee-91fa-7b9347f88aae";

    LoginAccount login(Map headers) throws Exception;

    HttpsURLConnection initializeRequest(String url, String method, String body);

    public String getResponseBody(HttpsURLConnection conn);

    String getBaseUrl();
    String getAccountId();
}