package com.jenscompany.actions;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.importexport.DefaultExportContext;
import com.atlassian.confluence.importexport.ImportExportException;
import com.atlassian.confluence.importexport.ImportExportManager;
import com.atlassian.confluence.importexport.impl.ExportScope;
import com.atlassian.confluence.importexport.resource.DownloadResourceManager;
import com.atlassian.confluence.importexport.resource.DownloadResourceReader;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.core.filters.ServletContextThreadLocal;
import com.atlassian.json.jsonorg.JSONArray;
import com.atlassian.xwork.ParameterSafe;
import com.jenscompany.api.DocuSignConnectionManager;
import com.jenscompany.impl.ConnectionUtilities;
import com.jenscompany.model.LoginAccount;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.json.simple.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.wsdl.Input;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Jen
 * Date: 28/10/15
 * Time: 18:48
 * To change this template use File | Settings | File Templates.
 */
public class TestConnection extends ConfluenceActionSupport {
    LoginAccount account = null;

    //@Autowired
    public DocuSignConnectionManager docuSignConnectionManager;
    public String envelopeId;
    public long pageId;
    public String recipientName;
    public String recipientEmail;

    PageManager pageManager;
    DownloadResourceManager downloadResourceManager;
    SettingsManager settingsManager;
    ImportExportManager importExportManager;

    public TestConnection() {
        /*
        try {
            docuSignConnectionManager.login(null);
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        baseUrl = docuSignConnectionManager.getBaseUrl();
        */
    }

    public String testLogin() {
        try {
            account = docuSignConnectionManager.login(null);
        } catch (Exception e) {
            addActionError(e.getMessage());
            return ERROR;
        }

        if(account == null) {
            addActionError("No DocuSign account found.");
            return ERROR;
        }

        return SUCCESS;
    }

    public String createEnvelope() {
        if(recipientName == null || recipientEmail == null) {
         addActionError("Either recipient name or email is missing");
         return ERROR;
         }

         if(pageId == 0) {
         addActionError("Please execute this action from a page. No page found.");
         return ERROR;
         }

         // Login to DocuSign
         testLogin();

         // Get PDF export of page
        DownloadResourceReader pagepdf = getPDF(pageId);
         /*String contextPath = getServletRequest().getContextPath();
         Page page = pageManager.getPage(pageId);
         String exportedDocument = null;
         DefaultExportContext exportContext = new DefaultExportContext();
         exportContext.setExportScope(ExportScope.PAGE);
         exportContext.setType(ImportExportManager.TYPE_PDF);
         exportContext.addWorkingEntity(page);
         exportContext.setUser(getRemoteUser());

         try {
         exportedDocument = importExportManager.exportAs(exportContext, null);
         } catch (ImportExportException e) {
         addActionError(e.getMessage());
         return ERROR;
         }
         */

         //  Setup recipient
         JSONObject recipient = new JSONObject();
         recipient.put("recipientId", "1");
         recipient.put("name", recipientName);
         recipient.put("email", recipientEmail);

         JSONArray signers = new JSONArray();
         signers.put(recipient);

         JSONObject recipients = new JSONObject();
         recipients.put("signers", signers);

         JSONObject document = new JSONObject();
         document.put("documentId", "1");
         document.put("name", "test.pdf");

         JSONArray documents = new JSONArray();
         documents.put(document);

         JSONObject envelope = new JSONObject();
         envelope.put("status", "sent");
         envelope.put("emailBlurb", "Here's the email body");
         envelope.put("emailSubject", "TEST SUBJECT");
         envelope.put("documents", documents);
         envelope.put("recipients", recipients);

         // grab a document
         byte[] bytes = new byte[(int) pagepdf.getContentLength()];

         try {
         InputStream inputStream = pagepdf.getStreamForReading();
             inputStream.read(bytes);
             inputStream.close();
         } catch (Exception e) {
            addActionError(e.getMessage());
            return ERROR;
         }

         // start constructing the multipart/form-data request...
         String requestBody = "\r\n\r\n--BOUNDARY\r\n" +
         "Content-Type: application/json\r\n" +
         "Content-Disposition: form-data\r\n" +
         "\r\n" +
         envelope.toString() +
         "\r\n\r\n--BOUNDARY\r\n" + 	// our xml formatted request body
         "Content-Type: application/pdf\r\n" +
         "Content-Disposition: file; filename=\"test.pdf\"; documentid=1\r\n" +
         "\r\n";
         // we break this up into two string since the PDF doc bytes go here and are not in string format.
         // see further below where we write to the outputstream...
         String reqBody2 = "\r\n" + "--BOUNDARY--\r\n\r\n";

         // write the body of the request...
         HttpsURLConnection conn = docuSignConnectionManager.initializeRequest(account.getBaseUrl() + "/envelopes", "POST", "");

         try {
         DataOutputStream dos = new DataOutputStream( conn.getOutputStream() );
         dos.writeBytes(requestBody.toString());
         dos.write(bytes);
         dos.writeBytes(reqBody2.toString());
         dos.flush();
         dos.close();
         } catch (Exception e) {
         addActionError(e.getMessage());
         addActionError("Error trying to write document to request.");
         return ERROR;
         }

         System.out.println("STEP 2:  Sending signature request on document...\n");

         int status = 0; // triggers the request

         try {
         status = conn.getResponseCode();
         } catch (Exception e) {
         addActionError(e.getMessage());
         return ERROR;
         }

         if( status != 201 )	// 201 = Created
         {
         addActionError(conn.getErrorStream().toString());
         return ERROR;
         }

         // Parse response
         String response = docuSignConnectionManager.getResponseBody(conn);

         try {
         JSONObject envresponse = ConnectionUtilities.decodeJSON(response);
         envelopeId = envresponse.get("envelopeId").toString();
         } catch (Exception e) {
         addActionError(e.getMessage());
         }

        return SUCCESS;
    }

    public String createDialog() {
        return SUCCESS;
    }

    protected HttpServletRequest getServletRequest()
    {
        return ServletContextThreadLocal.getRequest();
    }

    private DownloadResourceReader getPDF(long pageId) {
        try {
            String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
            URL url = new URL(baseUrl + "/spaces/flyingpdf/pdfpageexport.action?pageId=" + pageId
                    + "&os_username=admin&os_password=admin");
            URLConnection connection = url.openConnection();
            connection.connect();
            connection.getHeaderField("Location");
            URL redirecturl = connection.getURL();

            DownloadResourceReader downloadResourceReader = downloadResourceManager.getResourceReader(getRemoteUser().getName(), redirecturl.getPath(), new HashMap());
            return downloadResourceReader;
        } catch (Exception e) {
            addActionError("Exception while retrieving PDF File (login correct?): " + e.getMessage());
            return null;
        }

    }

    @ParameterSafe
    public String getEnvelopeId() {
        return envelopeId;
    }

    @ParameterSafe
    public long getPageId() {
        return pageId;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public DocuSignConnectionManager getDocuSignConnectionManager() {
        return docuSignConnectionManager;
    }

    public SettingsManager getSettingsManager() {
        return settingsManager;
    }

    @Override
    public void setSettingsManager(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    public DownloadResourceManager getDownloadResourceManager() {
        return downloadResourceManager;
    }

    public void setDownloadResourceManager(DownloadResourceManager downloadResourceManager) {
        this.downloadResourceManager = downloadResourceManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public void setImportExportManager(ImportExportManager importExportManager) {
        this.importExportManager = importExportManager;
    }

    public void setDocuSignConnectionManager(DocuSignConnectionManager docuSignConnectionManager) {
        this.docuSignConnectionManager = docuSignConnectionManager;
    }

}
